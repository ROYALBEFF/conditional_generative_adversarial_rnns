# Documentation
The code documentation is available in the HTML format. To get to the documentation's start page open `index.html` in
`/doc/build/html` with your web browser.

# Dependencies
- matplotlib (2.0.2)
- numpy (1.14.0)
- python (3.6.4)
- scikit-learn (0.19.1)
- tensorflow (1.5.0)

All experiments were conducted using the above packages.
There is no guarantee that the provided code in this repository works with older
package versions than the above-mentioned.

# License
This project is published under the MIT License
