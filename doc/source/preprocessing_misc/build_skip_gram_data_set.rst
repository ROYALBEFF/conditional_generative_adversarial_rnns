Building the Skip-gram data set
===============================

Description
-----------
Build the Skip-gram data set for a data set of texts.
This script was used for the Football Events experiments in
chapter **3.2.3 Experiments - Football Events Data - Experiment I** and
**3.2.4 Experiments - Football Events Data - Experiment II**.

The whole code can be found at `/src/preprocessing/build_skip_gram_data_set.py
<https://bitbucket.org/ROYALBEFF/conditional_generative_adversarial_rnns/src/master/src/preprocessing/build_skip_gram_data_set.py?at=master>`_.

Usage
-----
.. code-block:: bash

   build_skip_gram_data_set.py [-h] [-o PATH] [-n INTEGER] TEXTS_PATH

   Build Skip-gram data set from texts.

   positional arguments:
     TEXTS_PATH  path to txt file containing one sentence per line from which to
                 build the Skip-gram data set

   optional arguments:
     -h, --help  show this help message and exit
     -o PATH     path to tsv file used for saving the Skip-gram data set
                 (default: skip-gram.tsv)
     -n INTEGER  up to n history and future words are used for a target word
                 (default: 4)

