Word2Vec
========

Description
-----------
Skip-gram model for learning word embeddings.
The Skip-gram model is part of the Football Events experiments and described in
chapter **3.2.3 Experiments - Football Events Data - Experiment I**.

The whole code can be found at `/src/preprocessing/word2vec.py
<https://bitbucket.org/ROYALBEFF/conditional_generative_adversarial_rnns/src/master/src/preprocessing/word2vec.py?at=master>`_.

Usage
-----
.. code-block:: bash

    word2vec.py [-h] [--batchsize INTEGER] [--embeddingspath PATH]
                [--log PATH] [--epochs INTEGER] [--embeddingsize INTEGER]
                [-k INTEGER]
                FREQUENCY_PATH DATA_SET_PATH

    Learn word embeddings for given vocabulary.

    positional arguments:
      FREQUENCY_PATH        path to tsv file containing words and their
                            frequencies
      DATA_SET_PATH         path to tsv file containing the skip-gram data

    optional arguments:
      -h, --help            show this help message and exit
      --batchsize INTEGER   size of mini-batch (default: 128)
      --embeddingspath PATH
                            path to npy file containing the embeddings matrix
                            (default: embeddings.npy)
      --log PATH            path to file for logging time measurement (default:
                            log.txt)
      --epochs INTEGER      number of training iterations (default: 1000000)
      --embeddingsize INTEGER
                            dimensionality of word embeddings (default: 32)
      -k INTEGER            number of negative samples

Functions
---------
.. automodule:: word2vec
   :members:
