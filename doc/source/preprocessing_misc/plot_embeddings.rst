Plotting Embeddings
===================

Description
-----------
Plot learned embedding matrices.
This script was used for the Football Events experiments in
chapter **3.2.3 Experiments - Football Events Data - Experiment I**.

The whole code can be found at `/src/misc/plot_embeddings.py
<https://bitbucket.org/ROYALBEFF/conditional_generative_adversarial_rnns/src/master/src/misc/plot_embeddings.py?at=master>`_.

Usage
-----
.. code-block:: bash

    plot_embeddings.py [-h] [-o PATH] [-n INTEGER]
                          EMBEDDINGS_PATH FREQUENCY_PATH

   Plot word embeddings from npy file.

   positional arguments:
     EMBEDDINGS_PATH  path to npy file containing the embedding matrix
     FREQUENCY_PATH   path to tsv file containing words and their frequencies

   optional arguments:
     -h, --help       show this help message and exit
     -o PATH          write plot to the specified file (default: plot.png)
     -n INTEGER       number of words the plot will contain (default: all)

Functions
---------
.. automodule:: plot_embeddings
   :members:
