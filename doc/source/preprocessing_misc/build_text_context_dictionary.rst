Building the text-context dictionary
====================================

Description
-----------
Build the text-context dictionary for the Football Events data set.
This script was used for all Football Events experiments in section
**3.2 Football Events Data**.

The whole code can be found at `/src/preprocessing/build_text_context_dictionary.py
<https://bitbucket.org/ROYALBEFF/conditional_generative_adversarial_rnns/src/master/src/preprocessing/build_text_context_dictionary.py?at=master>`_.

Usage
-----
.. code-block:: bash

    build_text_context_dictionary.py [-h] [-o OUTPUT_PATH] [--nonames]
                                     [--etonly]
                                     FOOTBALL_EVENTS_PATH PLAYERS_PATH
                                     CLUBS_PATH

    Build the text context dictionary.

    positional arguments:
    FOOTBALL_EVENTS_PATH  path to football events csv file
    PLAYERS_PATH          path to tsv file containing all player names and their
                          total occurrence
    CLUBS_PATH            path to tsv file containing all club names and their
                          total occurrence

    optional arguments:
    -h, --help            show this help message and exit
    -o OUTPUT_PATH        path to tsv file for saving the dictionary (default:
                          text_context_dictionary.tsv)
    --nonames             replace names with placeholders
    --etonly              only use event types as context information

Functions
---------
.. automodule:: build_text_context_dictionary
    :members:
