GARNN
=====

Description
-----------
GARNN model for generating MNIST-like data.
This experiment is described in chapter **3.1 Experiments - MNIST**.

The whole code can be found at `/src/mnist/garnn.py
<https://bitbucket.org/ROYALBEFF/conditional_generative_adversarial_rnns/src/master/src/mnist/garnn.py?at=master>`_.

Usage
-----
.. code-block:: bash

    garnn.py [-h] [--batchsize INTEGER] [--epochs INTEGER]
             [--noisesize INTEGER] [--galpha FLOAT] [--dalpha FLOAT]
             [--stepsize INTEGER] [-k INTEGER]

    Run the GARNN model on the MNIST data set.

    optional arguments:
    -h, --help           show this help message and exit
    --batchsize INTEGER  size of mini-batch (default: 64)
    --epochs INTEGER     number of training iterations (default: 50000)
    --noisesize INTEGER  size of the random noise vector that is used as the
                       generator's input (default: 100)
    --galpha FLOAT       generator's learning rate (default: 0.001)
    --dalpha FLOAT       discriminator's learning rate (default: 0.001)
    --stepsize INTEGER   number of steps in which the data will be processed
                       (default: 4)
    -k INTEGER           number of discriminator's training steps per training
                       step of the generator


Functions
---------
.. automodule:: garnn
   :members:
