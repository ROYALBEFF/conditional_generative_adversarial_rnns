CGAN
====

Description
-----------
CGAN model for generating MNIST-like data.
This experiment is described in chapter **3.1 Experiments - MNIST**.

The whole code can be found at `/src/mnist/cgan.py
<https://bitbucket.org/ROYALBEFF/conditional_generative_adversarial_rnns/src/master/src/mnist/cgan.py?at=master>`_.

Usage
-----
.. code-block:: bash

    cgan.py [-h] [--batchsize INTEGER] [--epochs INTEGER]
            [--noisesize INTEGER] [--alpha FLOAT] [-k INTEGER]

    Run the CGAN model on the MNIST data set.

    optional arguments:
      -h, --help           show this help message and exit
      --batchsize INTEGER  size of mini-batch (default: 128)
      --epochs INTEGER     number of training iterations (default: 50000)
      --noisesize INTEGER  size of the random noise vector that is used as the
                           generator's input (default: 100)
      --alpha FLOAT        learning rate (default: 0.001)
      -k INTEGER           number of discriminator's training steps per training
                           step of the generator


Functions
---------
.. automodule:: cgan
   :members:
