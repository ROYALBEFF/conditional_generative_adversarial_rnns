GAN
===

Description
-----------
GAN model for generating MNIST-like data.
This experiment is described in chapter **3.1 Experiments - MNIST**.

The whole code can be found at `/src/mnist/gan.py
<https://bitbucket.org/ROYALBEFF/conditional_generative_adversarial_rnns/src/master/src/mnist/gan.py?at=master>`_.

Usage
-----
.. code-block:: bash

    gan.py [-h] [--batchsize INTEGER] [--epochs INTEGER]
           [--noisesize INTEGER] [--alpha FLOAT] [-k INTEGER]

    Run the GAN model on the MNIST data set.

    optional arguments:
    -h, --help           show this help message and exit
    --batchsize INTEGER  size of mini-batch (default: 64)
    --epochs INTEGER     number of training iterations (default: 50000)
    --noisesize INTEGER  size of the random noise vector that is used as the
                       generator's input (default: 100)
    --alpha FLOAT        learning rate (default: 0.001)
    -k INTEGER           number of discriminator's training steps per training
                       step of the generator


Functions
---------
.. automodule:: gan
   :members:
