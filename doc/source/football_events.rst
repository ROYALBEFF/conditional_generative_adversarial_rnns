Football Events Experiments
===========================

Description
-----------
This section contains documentations for the code that was used for the Football
Events experiments. The used data can be found in `/data/embeddings/` and `/data/football_events/preprocessed_data`.
The original data set can be found in `/data/football_events/events.csv`

.. toctree::
   :maxdepth: 1
   :caption: Football Events:

   football_events/cgarnn
   football_events/cgarnn_indices
   football_events/lstm
