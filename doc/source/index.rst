.. Conditional Generative Adversarial RNNs documentation master file, created by
   sphinx-quickstart on Sun Jan 28 17:34:29 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Conditional Generative Adversarial RNNs's documentation!
===================================================================

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   mnist
   football_events
   preprocessing_misc

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
