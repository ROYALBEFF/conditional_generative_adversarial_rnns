CGARNN - Embeddings
===================

Description
-----------
CGARNN model for generating Football event descriptions using word embeddings.
This experiment is described in chapter
**3.2.3 Experiments - Football Events Data - Experiment I** and
**3.2.4 Experiments - Football Events Data - Experiment II**.

The whole code can be found at `src/football_events/cgarnn_football_events.py
<https://bitbucket.org/ROYALBEFF/conditional_generative_adversarial_rnns/src/master/src/football_events/cgarnn_football_events.py?at=master>`_.

**NOTE:** This script often crashes due to a segmentation fault. Since there is
no way to access memory addresses directly, the error must be in one of the imported
packages that are using C bindings. Therefore we recommend using the snapshot function
and small iteration numbers to have as much intermediate results as possible.

Usage
-----
.. code-block:: bash

    cgarnn_football_events.py [-h] [--gweights PATH] [--gbias PATH]
                              [--dweights PATH] [--dbias PATH] [--snapshot]
                              [--fromdata BOOL] [--output PATH]
                              [--loss PATH] [--log PATH] [--epochs INTEGER]
                              [-k INTEGER] [--batchsize INTEGER]
                              [--glayers INTEGER] [--dlayers INTEGER]
                              [--galpha FLOAT] [--dalpha FLOAT]
                              FREQUENCY_PATH EMBEDDINGS_PATH DATA_SET_PATH

    Run the CGARNN model on the football events data with word embeddings.

    positional arguments:
      FREQUENCY_PATH       path to tsv file containing words and their frequencies
      EMBEDDINGS_PATH      path to npy file containing the embeddings matrix
      DATA_SET_PATH        path to tsv file containing texts and their
                           corresponding labels

    optional arguments:
      -h, --help           show this help message and exit
      --gweights PATH      path to npy file containing the generator's weights
      --gbias PATH         path to npy file containing the generator's bias
      --dweights PATH      path to npy file containing the discriminator's weights
      --dbias PATH         path to npy file containing the discriminator's bias
      --snapshot           save weights and biases after training
      --fromdata BOOL      if True, the random context will be drawn form the data
                           set rather then generating it (default: False)
      --output PATH        path to output file for generated sequences (default:
                           output.txt)
      --loss PATH          path to csv file for logging loss values (default:
                           loss.csv)
      --log PATH           path to file for logging time measurement (default:
                           log.txt)
      --epochs INTEGER     number of training iterations (default: 50000)
      -k INTEGER           number of discriminator's training steps per training
                           step of the generator (default: 1)
      --batchsize INTEGER  size of mini-batch (default: 64)
      --glayers INTEGER    number of generator's LSTM layers (default: 5)
      --dlayers INTEGER    number of discriminator's LSTM layers (default: 3)
      --galpha FLOAT       generator's learning rate (default: 0.001)
      --dalpha FLOAT       discriminator's learning rate (default: 0.001)


Functions
---------
.. automodule:: cgarnn_football_events
    :members:
