LSTM
====

Description
-----------
LSTM model for generating Football event descriptions.
This experiment is described in chapter
**3.2.7 Experiments - Football Events Data - Experiment V**.

The whole code can be found at `src/football_events/lstm_football_events.py
<https://bitbucket.org/ROYALBEFF/conditional_generative_adversarial_rnns/src/master/src/football_events/lstm_football_events.py?at=master>`_.

Usage
-----
.. code-block:: bash

   lstm_football_events.py [-h] [--output PATH] [--loss PATH] [--log PATH]
                                [--epochs INTEGER] [--batchsize INTEGER]
                                [--stepsize INTEGER] [--layers INTEGER]
                                [--alpha FLOAT]
                                FREQUENCY_PATH DATA_SET_PATH

   Run the LSTM model on the football events data.

   positional arguments:
     FREQUENCY_PATH       path to tsv file containing words and their frequencies
     DATA_SET_PATH        path to tsv file containing texts and their
                          corresponding labels

   optional arguments:
     -h, --help           show this help message and exit
     --output PATH        path to output file for generated sequences (default:
                          output.txt)
     --loss PATH          path to csv file for logging loss values (default:
                          loss.csv)
     --log PATH           path to file for logging time measurement (default:
                          log.txt)
     --epochs INTEGER     number of training iterations (default: 50000)
     --batchsize INTEGER  size of mini-batch (default: 64)
     --stepsize INTEGER   number of input elements for each prediction (default:
                          3)
     --layers INTEGER     number of LSTM layers (default: 3)
     --alpha FLOAT        learning rate (default: 0.001)

Functions
---------
.. automodule:: lstm_football_events
    :members:
