CGARNN - Indices
================

Description
-----------
CGARNN model for generating Football event descriptions using indices for word representation.
This experiment is described in chapter
**3.2.5 Experiments - Football Events Data - Experiment III** and
**3.2.6 Experiments - Football Events Data - Experiment IV**.

The whole code can be found at `src/football_events/cgarnn_football_events_indices.py
<https://bitbucket.org/ROYALBEFF/conditional_generative_adversarial_rnns/src/master/src/football_events/cgarnn_football_events_indices.py?at=master>`_.

Usage
-----
.. code-block:: bash

    cgarnn_football_events_indices.py [-h] [--gweights PATH] [--gbias PATH]
                                      [--dweights PATH] [--dbias PATH]
                                      [--snapshot] [--fromdata] [--etonly]
                                      [--output PATH] [--loss PATH]
                                      [--log PATH] [--epochs INTEGER]
                                      [-k INTEGER] [--batchsize INTEGER]
                                      [--glayers INTEGER]
                                      [--dlayers INTEGER] [--galpha FLOAT]
                                      [--dalpha FLOAT]
                                      FREQUENCY_PATH DATA_SET_PATH

    Run the CGARNN model on the football events data with indices representing the
    sequence elements.

    positional arguments:
    FREQUENCY_PATH       path to tsv file containing words and their frequencies
    DATA_SET_PATH        path to tsv file containing texts and their
                         corresponding labels.NOTE: if the data set does not
                         contain all 16 context variables, the random
                         contextvectors are drawn from the data set
                         automatically

    optional arguments:
    -h, --help           show this help message and exit
    --gweights PATH      path to npy file containing the generator's weights
    --gbias PATH         path to npy file containing the generator's bias
    --dweights PATH      path to npy file containing the discriminator's weights
    --dbias PATH         path to npy file containing the discriminator's bias
    --snapshot           save weights and biases after training
    --fromdata           if True, the random context will be drawn form the data
                         set rather then generating it (default: False)
    --etonly             only use this flag when the given data set contains
                         nothing but event_type and event_type2 as context
                         information. the context will then be represented as a
                         two-hot-vector
    --output PATH        path to output file for generated sequences (default:
                         output.txt)
    --loss PATH          path to csv file for logging loss values (default:
                         loss.csv)
    --log PATH           path to file for logging time measurement (default:
                         log.txt)
    --epochs INTEGER     number of training iterations (default: 50000)
    -k INTEGER           number of discriminator's training steps per training
                         step of the generator (default: 1)
    --batchsize INTEGER  size of mini-batch (default: 64)
    --glayers INTEGER    number of generator's LSTM layers (default: 5)
    --dlayers INTEGER    number of discriminator's LSTM layers (default: 3)
    --galpha FLOAT       generator's learning rate (default: 0.001)
    --dalpha FLOAT       discriminator's learning rate (default: 0.001)


Functions
---------
.. automodule:: cgarnn_football_events_indices
    :members:
