import argparse
import bisect
import csv
import math
import matplotlib.pyplot as plt
import numpy as np
import random
import re
import tensorflow as tf
import time


def batch(batch_size, data_set, frequencies):
    '''
    Get batch of target and context words.

    The batch is a tuple containing two arrays, one for the target and another one for the context words.
    Words that occur often in the data set got a high probability to be subsampled. Therefore the frequencies
    dictionary is needed.

    :param int batch_size:
        Number of elements the batch should contain.
    :param list data_set:
        List from where to draw the batch elements.

        List contains tuples (*tw*, *cw*) where *tw* is a target word and *cw* a list of context words. All words are
        represented by integer values.
    :param dict frequencies:
        Dictionary that maps each word (represented by an index) in the data set to it's relative occurrence. This
        dictionary is needed for subsampling words that occur often.
    :return:
        Batch (two numpy arrays) containing batch_size elements. The first array contains all the target words, whereas
        the second one contains all the context words.
    '''

    # dictionary containing subsampling information for all words that were observed in this iteration
    subsampling = dict()
    batch = list()
    indices = list()
    size = 0

    while size < batch_size:
        i = random.randrange(0, len(data_set))
        while elem(i, indices):
            i = random.randrange(0, len(data_set))
        bisect.insort(indices, i)

        (tw, cws) = data_set[i]
        # if the target word wasn't observed in this batch yet, decide whether to subsample this word
        if subsampling.get(tw) is None:
            subsampling[tw] = subsample(tw, frequencies)

        # for all context words that weren't observed in this batch yet, decide whether to subsample them
        for cw in cws:
            if subsampling.get(cw) is None:
                subsampling[cw] = subsample(cw, frequencies)

        # if the chosen target word is subsampled, continue with the next one
        if subsampling[tw]:
            continue
        else:
            # remove all subsampled context words
            cws = list(filter(lambda cw: not subsampling[cw], cws))
            # if all context words were subsampled, continue with next target word
            if not cws:
                continue
            # choose one of the remaining context words randomly
            c = random.randint(0, len(cws) - 1)
            batch.append((tw, [cws[c]]))

            # increase current batch size by one, if the expected batch size is reached, stop adding new elements to it
            size = size + 1

    # transform list of tuples to a tuple containing two lists ([target_words], [context_words])
    (xs, ys) = unzip(batch)
    return (np.asarray(xs), np.asarray(ys))


def data_set(fname, dict):
    '''
    Load data set from file into a list of index tuples, where the mapping is given by dict.
    Each tuple contains a target word and all it's context words, whereas the context words are nearby words in range
    context_size.

    :param str fname:
        Path to file containing the data_set.
        The file must contain tab-separated values, one data pair per line.
    :param dict dict:
        Dictionary for word-index mapping.
    :return:
        List of tuples (target word, context words) for all target words.
        The tuples don't contain the words themselves but their corresponding indices given by the dictionary.
    '''

    with open(fname, 'r') as f:
        data_set = list()
        reader = csv.reader(f, delimiter='\t')
        for tuple in reader:
            data_set.append((dict[tuple[0]], list(map(lambda e: dict[e], tuple[1:]))))
        f.close()
    return data_set


def elem(x, xs):
    '''
    Checks if *x* is element of list *xs*. *xs* must be a sorted list.

    :param a x:
        Element to search in the list.
    :param [a] xs:
        Sorted list in which *x* is searched.
    :return:
        A boolean value, telling whether *x* is element of *xs*.
    '''
    i = bisect.bisect_left(xs, x)
    return i != len(xs) and xs[i] == x


def frequencies(fname):
    '''
    Read vocabulary containing words and punctuation characters and their relative occurrences from file.

    :param str fname:
        Path to file.
        Expects a tsv file where the first column contains all the tokens and the second column contains
        the frequency. It's expected that the tokens are already sorted descending by their occurrence.

    :return:
        Tuple of two lists, whereas the first list contains all the entries from column one and the second contains
        all the entries from column two.

    '''
    with open(fname) as f:
        reader = csv.reader(f, delimiter='\t')
        (tokens, frequencies) = unzip(map(tuple, reader))
        f.close()
    return (tokens, map(float, frequencies))


def lines(fname):
    '''
    Read all lines from given file and remove newline characters.

    :param str fname:
        Path to file.
    :return:
        List of all lines of given file with newline characters removed.
    '''
    with open(fname) as f:
        texts = f.readlines()
        f.close()
    # remove line feed characters
    texts = list(map(lambda t: t.rstrip('\n'), texts))
    return texts


def plot_embeddings(embeddings, words, reverse_dictionary, fname):
    '''
    Plots 2-dimensional representation of learned word embeddings.

    :param np.array embeddings:
        2-dimensional representation of embeddings.

        Embeddings dimension must be changed in order to make plotting possible.
    :param [int] words:
        List of indices representing words to be included in the plot. These words are used as labels for the
        plotted points.
    :param dict reverse_dictionary:
        Dictionary that maps indices to there corresponding word.
    :param str fname:
        Path to file in which the plot will be saved.
    :return:
        *None*
    '''

    # prepare plot of size 32 x 32
    plt.figure(figsize=(32, 32))
    for word in words:
        # get 2-dimensional vector representation of the current word
        (x, y) = embeddings[word]
        # add (x, y) to plot
        plt.scatter(x, y)
        # add word as label to point (x, y)
        plt.annotate(reverse_dictionary[word], xy=(x, y), xytext=(5, 2), textcoords='offset points', ha='right', va='bottom')

    plt.savefig(fname)


def subsample(word, frequencies):
    '''
    Takes a index, which represents a certain word and returns a boolean value telling whether to subsample this word.
    Therefore the frequency of that word is used. Frequent words are more likely to be subsampled than words that
    occur rarely.

    :param str word:
        Index representing the word to predict the subsample rate for.
    :param dict frequencies:
        Dictionary that maps words to there relative frequency among all the other words.
    :return:
        Boolean value telling whether to subsample the given word.
    '''

    z = frequencies[word]
    # probability of keeping the given word
    p = (math.sqrt(z / 0.001) + 1) * (0.001 / z)
    # random value between 0 and 1
    t = random.random()
    return p <= t


def tokens(texts):
    '''
    Read all words and punctuation characters from list of strings.

    :param [str] texts:
        List of strings.
    :return:
        List of all words and punctuation characters that occur in texts.
    '''

    tokens = list()
    for text in texts:
        tokens.append(re.findall(r"[\w']+|[$.,!?;()]", text))
    return tokens


def unzip(tuples):
    '''
    Takes a list of tuples and returns two list, one containing all the first elements and another containing all
    the second elements of the tuples.

    :param [(a,b)] tuples:
        List of tuples.
    :return:
        Two list, one containing all the first elements and another containing all the second elements of the tuples.
    '''

    tuples = list(zip(*tuples))
    return (list(tuples[0]), list(tuples[1]))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Learn word embeddings for given vocabulary.")
    parser.add_argument("vocabulary_path", metavar="FREQUENCY_PATH", type=str,
                        help="path to tsv file containing words and their frequencies")
    parser.add_argument("data_set_path", metavar="DATA_SET_PATH", type=str,
                        help="path to tsv file containing the skip-gram data")
    parser.add_argument("--batchsize", metavar="INTEGER", default=128, type=int,
                        help="size of mini-batch (default: 128)")
    parser.add_argument("--embeddingspath", metavar="PATH", default="embeddings.npy", type=str,
                        help="path to npy file containing the embeddings matrix (default: embeddings.npy)")
    parser.add_argument("--log", metavar="PATH", default="log.txt", type=str,
                        help="path to file for logging time measurement (default: log.txt)")
    parser.add_argument("--epochs", metavar="INTEGER", default=1000000, type=int,
                        help="number of training iterations (default: 1000000)")
    parser.add_argument("--embeddingsize", metavar="INTEGER", default=32, type=int,
                        help="dimensionality of word embeddings (default: 32)")
    parser.add_argument("-k", metavar="INTEGER", default=18, type=int,
                        help="number of negative samples")
    args = parser.parse_args()

    # path to tsv file containing all tokens and their relative occurrences
    frequencies_path = args.vocabulary_path
    # path to tsv file containing skip gram data
    data_set_path = args.data_set_path
    # path to txt file for saving loss values and training time
    log_path = args.log
    # path to npy file for saving the embeddings as a numpy array
    embeddings_path = args.embeddingspath
    # number of training iterations
    epochs = args.epochs
    # embeddings dimensionality
    embedding_size = args.embeddingsize
    # size of mini-batch
    batch_size = args.batchsize
    # number of negative samples
    k = args.k

    print("Loading vocabulary... ", end='')
    # list of all words in file, sorted by their frequency
    (vocab, freqs) = frequencies(frequencies_path)
    print("Done!")

    print("Building dictionaries... ", end='')
    # maps words to indices
    dictionary = dict()
    for word in vocab:
        dictionary[word] = len(dictionary)
    # maps indices to words
    reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
    # maps indices to frequencies
    frequencies = dict(zip(range(len(dictionary)), freqs))
    print("Done!")

    print("Deleting vocabulary... ", end='')
    # delete vocabulary and frequency lists, because they're no longer needed
    del vocab
    del freqs
    print("Done!")

    print("Loading data set... ", end='', flush=True)
    vocabulary_size = len(dictionary)
    # data set containing all text from given file, where all words and punctuation characters are represented by their
    # corresponding index
    data_set = data_set(data_set_path, dictionary)
    print("Done!")

    print("Building network graph... ", end='')
    # projection matrix to embed words (map a word to it's vector representation)
    # input = one hot vector of vocabulary size
    # result = row (vector representation) corresponding to the word represented by the one hot vector
    embeddings = tf.Variable(tf.random_uniform([vocabulary_size, embedding_size], -1, 1))

    # theta - parameters for noise-contrastive estimation (binary logistic regression)
    # truncated normal distribution:
    # The generated values follow a normal distribution with specified mean and standard deviation,
    # except that values whose magnitude is more than 2 standard deviations from the mean are dropped and re-picked.
    nce_weights = tf.Variable(tf.truncated_normal([vocabulary_size, embedding_size], stddev=1 / math.sqrt(embedding_size)))
    nce_biases = tf.Variable(tf.zeros([vocabulary_size]))

    # context words
    inputs = tf.placeholder(tf.int32, shape=[batch_size])
    # target word to predict
    # must be a tensor of rank 2 and shape [batch_size, num_true] where num_true is the number
    # of target classes per training example
    labels = tf.placeholder(tf.int32, shape=[batch_size, 1])

    # get vector representations from projection matrix for each word in batch
    # the embedding_lookup function makes representing words as one hot vectors unnecessary
    embed = tf.nn.embedding_lookup(embeddings, inputs)

    # generate k negative samples
    # project each negative sample to a word vector representation using nce_weights and nce_biases
    # objective function: log_sig(tw, cw) + sum_k(log_sig(sample, cw))
    # embed will NOT be multiplied with the nce_weights but is needed for the objective function
    loss = tf.reduce_mean(tf.nn.nce_loss(nce_weights, nce_biases, labels, embed, k, vocabulary_size))
    optimizer = tf.train.GradientDescentOptimizer(learning_rate=1).minimize(loss)
    print("Done!")

    print("Initializing variables... ", end='')
    # start session and initialise variables
    sess = tf.InteractiveSession()
    tf.global_variables_initializer().run()
    print("Done!")

    print("Learning word embeddings... ", end='', flush=True)
    log_file = open(log_path, "w+")
    # start time measurement
    t_start = time.time()

    for e in range(epochs):
        (xs, ys) = batch(batch_size, data_set, frequencies)
        sess.run(optimizer, {inputs: xs, labels: ys})

        if e % 1000 == 0:
            loss_value = sess.run(loss, {inputs: xs, labels: ys})
            log_file.write("Epoch: %d - Loss: %.3f\n" % (e, loss_value))

    # end time measurement
    t_end = time.time()
    duration = t_end - t_start
    log_file.write("Training duration: %.3f\n" % duration)
    print("Done!")

    print("Saving embeddings... ", end='')
    embeddings_array = embeddings.eval()
    np.save(embeddings_path, embeddings_array, False, False)
    print("Done!")

    exit(0)