import argparse
import bisect
import csv
import itertools
import re


def elem(x, xs):
    '''
    Checks if x is element of list xs. xs must be a sorted list.

    :param x:
        Element to search in the list.
    :param xs:
        Sorted list in which x is searched.
    :return:
        A boolean value, telling whether x is element of xs.
    '''
    i = bisect.bisect_left(xs, x)
    return i != len(xs) and xs[i] == x


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Build the text context dictionary.")
    parser.add_argument("football_events", metavar="FOOTBALL_EVENTS_PATH", type=str,
                        help="path to football events csv file")
    parser.add_argument("players", metavar="PLAYERS_PATH", type=str,
                        help="path to tsv file containing all player names and their total occurrence")
    parser.add_argument("clubs", metavar="CLUBS_PATH", type=str,
                        help="path to tsv file containing all club names and their total occurrence")
    parser.add_argument("-o", metavar="OUTPUT_PATH", type=str, default="text_context_dictionary.tsv",
                        help="path to tsv file for saving the dictionary (default: text_context_dictionary.tsv)")
    parser.add_argument("--nonames", action="store_true", help="replace names with placeholders")
    parser.add_argument("--etonly", action="store_true", help="only use event types as context information")
    args = parser.parse_args()

    football_events_path = args.football_events
    players_path = args.players
    clubs_path = args.clubs
    output_path = args.o
    no_names = args.nonames
    event_types_only = args.etonly

    print("Loading data... ", end='')
    # read csv file
    with open(football_events_path, 'r') as file:
        reader = csv.reader(file)
        data = list(reader)
    file.close()

    # load list of player names
    with open(players_path, 'r') as file:
        reader = csv.reader(file, delimiter='\t')
        # first entry in file is "NA" which will be mapped to -1
        players = dict(reader)
        player_list = list()
        if no_names:
            # list of all football players that occur in the data, initialised with names that do not occur in the player columns
            player_list = ["abdon", "bacari", "bagadur", "champagne", "djigla", "ejike", "favilli", "isi", "nereo",
                           "prats", "tarsi", "uzoenyi"]
            for p in players.keys():
                names = re.findall(r"[\w']+|[$.,!?;()]", p)
                for name in names:
                    if name != "na" and not elem(name, player_list):
                        bisect.insort(player_list, name)

    file.close()

    # load list of club names
    with open(clubs_path, 'r') as file:
        reader = csv.reader(file, delimiter='\t')
        # first entry in file is "NA" which will be mapped to -1
        clubs = dict(reader)
        club_list = list()
        if no_names:
            club_list = ["05", "07", "1899", "albion", "bromwich", "cf", "club", "fsv", "hamburger", "hotspur", "inter",
                         "koln", "munchen", "queens", "rangers", "sport"]
            for c in clubs.keys():
                names = re.findall(r"[\w']+|[$.,!?;()]", c)
                for name in names:
                    if name != "na" and not elem(name, club_list):
                        bisect.insort(club_list, name)
    file.close()
    print("Done!")

    print("Loading texts and context information... ", end='', flush=True)
    tuples = list()
    for row in data[1:]:
        # column 4 contains the event description
        text = row[4].lower() + "$"

        if no_names:
            # separate the text into words and punctuation characters
            words = re.findall(r"[\w']+|[$.,!?;()]", text)

            for (i, word) in enumerate(words):
                if elem(word, player_list):
                    words[i] = "PLAYER"
                elif elem(word, club_list):
                    words[i] = "CLUB"

            # remove consecutive duplicates, they are originated while replacing names with placeholders
            words = map(lambda w: w[0], itertools.groupby(words))
            # join words to sentence and add it to the list of texts
            text = ' '.join(words)

        columns_events = [5, 6]
        context_events = list()
        for i in columns_events:
            c = row[i]
            # if the information is not available use -1
            if c == "NA":
                context_events = context_events + [-1]
            else:
                context_events = context_events + [int(c)]

        context_clubs = list()
        context_players = list()
        context_misc = list()
        if not event_types_only:
            # columns 5,6 and 8 to 22 contain all the context information for the event description
            columns_clubs = [8, 9]
            context_clubs = list(map(lambda i: int(clubs[row[i].lower()]), columns_clubs))

            columns_players = list(range(10,14))
            context_players = list(map(lambda i: int(players[row[i].lower()]), columns_players))

            columns_misc = list(range(14, 22))
            context_misc = list()
            for i in columns_misc:
                c = row[i]
                # if the information is not available use -1
                if c == "NA":
                    context_misc = context_misc + [-1]
                else:
                    context_misc = context_misc + [int(c)]


        context = context_events + context_clubs + context_players + context_misc
        tuples.append((text, context))
    print("Done!")

    print("Writing data to file... ", end='', flush=True)
    # write dictionary to tsv file
    with open(output_path, 'w+') as f:
        writer = csv.writer(f, delimiter='\t')
        # value for each entry in the dictionary is a list. this way the brackets won't be written to the tsv file
        items = [tuple([text, *context]) for (text, context) in tuples]
        writer.writerows(items)
    f.close()
    print("Done!")

    exit(0)
