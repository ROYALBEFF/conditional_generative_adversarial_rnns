import argparse
import csv
import re


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Build Skip-gram data set from texts.")
    parser.add_argument("texts", metavar="TEXTS_PATH", type=str,
                        help="path to txt file containing one sentence per line from which to build the Skip-gram data set")
    parser.add_argument("-o", metavar="PATH", default="skip-gram.tsv", type=str,
                        help="path to tsv file used for saving the Skip-gram data set (default: skip-gram.tsv)")
    parser.add_argument("-n", metavar="INTEGER", default=4, type=str,
                        help="up to n history and future words are used for a target word (default: 4)")
    args = parser.parse_args()

    # path to file containing the texts from which to build the skip-gram data set
    input_file = args.texts
    # path to tsv file for saving the skip-gram data set
    output_file = args.o
    # number of context words around a target word (window size)
    context_size = args.n

    if context_size <= 0:
        print("Negative context size! Using default.")
        context_size = 4

    print("Building Skip-gram data set... ", end='')
    with open(output_file, 'w') as output:
        writer = csv.writer(output, delimiter='\t')
        with open(input_file, 'r') as input:
            texts = input.readlines()

            for text in texts:
                tokens = list()
                # find all tokens in text
                tokens.append(re.findall(r"[\w']+|[$.,!?;()]", text))

                # build skip-gram data regarding the context size and write it to the tsv-file
                for token in tokens:
                    for (i, tw) in enumerate(token):
                        lower_bound = max([0, i - context_size])
                        upper_bound = i + context_size + 1
                        data = [tw] + token[lower_bound:i] + token[i + 1:upper_bound]
                        writer.writerow(data)
            input.close()
        output.close()
    print("Done!")
    exit(0)