import argparse
import csv
from functools import reduce
import numpy as np
import random
import re
import tensorflow as tf
import tensorflow.contrib.rnn as rnn
import time


def batch(step_size, batch_size, data, random_start=True):
    """
    Draws random batch from given data set.

    :param int step_size:
        Length of drawn subsequences.
    :param int batch_size:
        Number of drawn subsequences.
    :param np.array data:
        Data set from where to draw the subsequences.
    :param bool random_start:
        Describes if the drawn subsequences should start at a random position or at the very beginning of the sequence.
        (Default: True)
    :return:
        Triple (**xs**, **ps**, **ys**) where

        - **xs** of shape [step_size, batch_size, 1] containing batch_size sequences of length step_size
        - **ps** word to predict for each batch
        - **ys** corresponding context information for each batch and consists of integer tuples of length 16.
    """
    # randomize the data order and then draw the first batch_size elements
    np.random.shuffle(data)
    batch = data[:batch_size]
    (xs, ps, ys) = (list(), list(), list())
    for (i, (x, y)) in enumerate(batch):
        # pad with 1's ($) in case that x is does not contain enough elements
        x_ = x + ([1] * (step_size + 2))
        # in case of x not containing enough elements, always start at 0
        if len(x) >= step_size + 2 and random_start:
            start = random.randint(0, len(x_) - (step_size + 2))
        else:
            start = 0
        end = start + step_size
        ps.append(x_[end + 1])
        xs.append(x_[start:end])
        ys.append(y)

    return (np.expand_dims(np.swapaxes(xs, 0, 1), 2), one_hot_vectors(ps, dictionary_size), ys)


def one_hot_vectors(indices, size):
    '''
    Generate one hot vectors with 1's at the given indices where size defines the vector length.

    :param [int] indices:
        List of indices.
    :param int size:
        Vector length.
    :return:
        Array containing one hot vectors.
    '''
    n = len(indices)
    vector = np.zeros((n, size))
    vector[np.arange(n), indices] = 1
    return vector


def data_set(data_set_path, dictionary):
    """
    Loads data set from tsv-file.

    :param str data_set_path:
        Path to tsv file containing the data set. The first column of the data set contains the texts and the remaining
        ones the context information.
    :param dict dictionary:
        Mapping of words (str) to indices (int).
    :return:
        Numpy array containing tuples where the first entry is a list of integers representing the text and the second
        entry is a tuple of 16 integer values representing the corresponding context.
    """
    with open(data_set_path) as f:
        # read data from file
        reader = csv.reader(f, delimiter='\t')
        data_set = list(reader)
        # map words and punctuation characters to their corresponding indices
        texts = [list(map(lambda t: dictionary[t], tokens(entry[0]))) for entry in data_set]
        # parse context vectors from file
        contexts = [tuple(map(int, entry[1:])) for entry in data_set]
        data_set_tuples = list(zip(texts, contexts))
        f.close()

    return np.array(data_set_tuples)


def LSTM(x, y, W, b, layers):
    '''
    Multi layer LSTM network.
    Computes probability of the next word with regard to the input sequence *x* and the context vector *y*.

    :param tf.Tensor x:
        Tensor containing *batch_size* input sequences of length *step_size*.
    :param tf.Tensor y:
        Tensor representing the context information for *x*.
    :param tf.Tensor W:
        Weights for output layer.
    :param tf.Tensor b:
        Bias for output layer.
    :param int layers:
        Number of LSTM layers.
    :return:
        Tuple containing network's output after each step and the prediction of the next word.
    '''
    # get list of batches for each step and concatenate the context vector to each input
    x = tf.unstack(x)
    x = [tf.concat([x_, y], 1) for x_ in x]

    # create multi layer LSTM
    multi_lstm_cell = rnn.MultiRNNCell([rnn.BasicLSTMCell(1 + 16)] * layers)

    # get networks prediction
    (h, _) = rnn.static_rnn(multi_lstm_cell, x, dtype=tf.float32)
    # return output after each step and next word prediction
    return (h, tf.matmul(h[-1], W) + b)


def tokens(text):
    '''
    Read all words and punctuation characters from string.

    :param str text:
        Text.
    :return:
        List of all words and punctuation characters that occur in the text.
    '''
    tokens = re.findall(r"[\w']+|[$.,!?;()]", text)
    return tokens


def unzip(tuples):
    '''
    Takes an array of tuples and returns two arrays, one containing all the first elements and another containing all
    the second elements of the tuples.

    :param np.array tuples:
        Array of tuples.
    :return:
        Two arrays, one containing all the first elements and another containing all the second elements of the tuples.
    '''
    tuples = list(zip(*tuples))
    return (np.array(tuples[0]), np.array(tuples[1]))


def vec2seq(indices, reverse_dictionary):
    """
    Maps an array of indices to their corresponding words.

    :param np.array indices:
        Array containing indices. The array represents batch_size sentences.
    :param dict reverse_dictionary:
        Dictionary mapping indices to their corresponding words.
    :return:
        List of strings that are represented by the input indices.
    """
    (sequence_length, batch_size, _) = np.shape(indices)
    # initialize batch_size strings
    sequences = [""] * batch_size
    # extract all words for each batch and build sentences
    for i in range(sequence_length):
        for j in range(batch_size):
            index = np.argmax(indices[i][j])
            word = reverse_dictionary[index]
            sequences[j] = sequences[j] + " " + word

    return sequences


def vocabulary(vocabulary_path):
    '''
    Read vocabulary containing words and punctuation characters from file.

    :param str vocabulary_path:
        Path to file.
        Expects a tsv file where the first column contains all the tokens and the second column contains
        the frequency, even if the frequency is not needed.
    :return:
        List of all words in vocabulary.
    '''
    with open(vocabulary_path) as f:
        reader = csv.reader(f, delimiter='\t')
        (tokens, _) = unzip(map(tuple, reader))
        f.close()
    return tokens


def zipWith(f, *ls):
    '''
    Zip *n* lists using *n*-dimensional function f instead of tuples.
    :param f:
        *n*-dimensional function.
    :param ls:
        *n* lists. The lists will be zipped using *f*
    :return:
        List containing results of *f* applied to the elements of *ls*
    '''
    return [f(*t) for t in zip(*ls)]


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Run the LSTM model on the football events data.")
    parser.add_argument("vocabulary_path", metavar="FREQUENCY_PATH", type=str,
                        help="path to tsv file containing words and their frequencies")
    parser.add_argument("data_set_path", metavar="DATA_SET_PATH", type=str,
                        help="path to tsv file containing texts and their corresponding labels")
    parser.add_argument("--output", metavar="PATH", default="output.txt", type=str,
                        help="path to output file for generated sequences (default: output.txt)")
    parser.add_argument("--loss", metavar="PATH", default="loss.csv", type=str,
                        help="path to csv file for logging loss values (default: loss.csv)")
    parser.add_argument("--log", metavar="PATH", default="log.txt", type=str,
                        help="path to file for logging time measurement (default: log.txt)")
    parser.add_argument("--epochs", metavar="INTEGER", default=50000, type=int,
                        help="number of training iterations (default: 50000)")
    parser.add_argument("--batchsize", metavar="INTEGER", default=64, type=int,
                        help="size of mini-batch (default: 64)")
    parser.add_argument("--stepsize", metavar="INTEGER", default=3, type=int,
                        help="number of input elements for each prediction (default: 3)")
    parser.add_argument("--layers", metavar="INTEGER", default=3, type=int,
                        help="number of LSTM layers (default: 3)")
    parser.add_argument("--alpha", metavar="FLOAT", default=0.001, type=float,
                        help="learning rate (default: 0.001)")

    args = parser.parse_args()

    # path to file containing all words of the data set and their frequencies
    vocabulary_path = args.vocabulary_path
    # path to tsv file containing the football event texts and their labels
    data_set_path = args.data_set_path
    # path to text file for generated sequences
    output_path = args.output
    # path to csv file for logging loss values
    loss_path = args.loss
    # path to text file for logging time measurement
    log_path = args.log
    # number of training iterations
    # ca 37 secs for 1000 epochs
    epochs = args.epochs
    # size of mini-batch
    batch_size = args.batchsize
    # number of input elements for each prediction
    step_size = args.stepsize
    # number of LSTM layers
    layers = args.layers
    # learning rate
    alpha = args.alpha

    print("Loading vocabulary... ", end='')
    # list of all words in file
    vocab = vocabulary(vocabulary_path)
    print("Done!")

    print("Building vocabulary dictionaries... ", end='')
    # maps words to indices
    dictionary = dict()
    for word in vocab:
        dictionary[word] = len(dictionary)
    # maps indices to words
    reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
    dictionary_size = len(dictionary)
    print("Done!")

    print("Deleting vocabulary... ", end='')
    del vocab
    print("Done!")

    print("Loading data set... ", end='')
    data = data_set(data_set_path, dictionary)
    print("Done!")

    print("Building network graphs... ", end='')
    # Xavier variable initializer and random seed
    seed = 611357224
    xavier = tf.glorot_normal_initializer(seed=seed)

    # Discriminator's input. None means that there can be an arbitrary number of input vectors. Each input vector must be of
    # size embeddings_size, because the words are represented by vectors and their size is determined by the embedding
    # matrix.
    x = tf.placeholder(tf.float32, [step_size, batch_size, 1], "x")
    # Context input for both generator and discriminator. None means that there can be an arbitrary number of context
    # vectors. Each context vector must be of size 16 because there are 16 entries each represented by a numerical value
    # in the data set that form the context.
    y = tf.placeholder(tf.float32, [batch_size, 16], "y")
    # placeholder for next word to predict
    p = tf.placeholder(tf.float32, [batch_size, dictionary_size])

    # Weights and biases for LSTM output layer
    W = tf.get_variable("W", [1 + 16, dictionary_size], initializer=xavier)
    b = tf.Variable(tf.zeros([dictionary_size]), name="b")

    # Generator's output sequence which is not mapped to discrete vector space of the word embeddings
    (predictions, next_word) = LSTM(x, y, W, b, layers)
    loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=next_word, labels=p))
    optimizer = tf.train.AdamOptimizer(learning_rate=alpha).minimize(loss)

    # compare highest rated words and actual next words in the batch
    diff = tf.equal(tf.argmax(next_word, 1), tf.argmax(p, 1))
    # calculate prediction accuracy
    accuracy = tf.reduce_mean(tf.cast(diff, tf.float32))
    print("Done!")

    print("Initializing variables... ", end='')
    sess = tf.InteractiveSession()
    tf.global_variables_initializer().run()
    print("Done!")

    print("Training network...\n")
    loss_file = open(loss_path, 'w+')

    # start time measurement
    t_start = time.time()

    for e in range(epochs):
        (xs, ps, ys) = batch(step_size, batch_size, data)
        sess.run(optimizer, {x: xs, p: ps, y: ys})

        # log loss and generated sequences every 1000th iteration
        if e % 1000 == 0:
            # print accuracy and loss for each output vector
            (accuracy_value, loss_value) = sess.run([accuracy, loss], {x: xs, p: ps, y: ys})
            print("Epoch " + str(e) + ", Minibatch Loss= " + \
                  "{:.6f}".format(loss_value) + ", Training Accuracy= " + \
                  "{:.5f}".format(accuracy_value))
            loss_file.write("%d, %f\n" % (e, loss_value))

    # stop time measurement
    t_end = time.time()
    t_duration = t_end - t_start
    print("Done!")

    loss_file.close()

    print("Saving training duration...", end='')
    with open(log_path, 'w+') as log_file:
        t_duration_secs = t_duration
        log_file.write("training duration: %f seconds" % t_duration_secs)
    log_file.close()
    print("Done!")

    print("Generating test sequences...", end='')
    # draw random batch from data set
    (xs, ps, ys) = batch(step_size, batch_size, data, False)
    # squeeze words to change shape [step_size, batch_size, 1] to [step_size, batch_size]
    squeezed_xs = np.squeeze(xs)
    # reshape from [step_size, batch_size] to [batch_size, step_size]
    swapped_xs = np.swapaxes(squeezed_xs, 0, 1)
    # get words from all batches and start building strings
    texts = [reduce(lambda acc, v: acc + " " + reverse_dictionary[v], x, "") for x in swapped_xs]
    # generate sequences of length 54, which is the maximum length in the data set
    for _ in range(53):
        # run LSTM and get output for all steps
        prediction = sess.run(next_word, {x: xs, y: ys})
        prediction = (tf.argmax(prediction, 1)).eval()
        # remove first words and add new predicted words
        squeezed_xs = np.concatenate([squeezed_xs[1:], np.expand_dims(prediction, 0)])
        # expand shape [step_size, batch_size] to [step_size, batch_size, 1]
        xs = np.expand_dims(squeezed_xs, 2)
        # add new words to the texts
        words = [reverse_dictionary[p] + " " for p in prediction]
        texts = zipWith(lambda t, w: t + " " + w, texts, words)
    print("Done!")

    print("Saving generated sequences...", end='')
    output_file = open(output_path, 'w+')
    for text in texts:
        output_file.write(text + "\n")
    output_file.close()
    print("Done!")

    exit(0)
