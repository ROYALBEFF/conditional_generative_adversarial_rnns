import argparse
import csv
from functools import reduce
from operator import mul
import numpy as np
import random
import re
import tensorflow as tf
import tensorflow.contrib.rnn as rnn
import time


def batch(batch_size, data, event_types_only):
    """
    Draws random batch from given data set.

    :param int batch_size:
        Number of samples.
    :param np.array data:
        Data set from where to draw the samples.
    :param bool event_types_only:
        If this is true the context vectors will be represented as two-hot-vectors.
        The context vector must not consist of other variables than the event types.
    :return:
        Tuple of arrays. The first array contains padded texts.
        The second array contains the corresponding context vectors.
    """
    # randomize the data order and then draw the first batch_size elements
    np.random.shuffle(data)
    batch = data[:batch_size]
    # pad sequences with ones to a length of sequence_length
    for (i, (x, y)) in enumerate(batch):
        x = np.pad(x, (0, 54 - len(x)), 'constant', constant_values=1)
        batch[i] = (x, y)
    # divide data in two arrays: (texts, contexts)
    (xs, ys) = unzip(batch)
    xs = np.swapaxes(xs, 0, 1)
    xs = one_hot_vectors(xs.flatten(), xs.shape, dictionary_size)
    # represent context as two-hot-vector
    if event_types_only:
        ys = event_type_vectors(ys)

    return (xs, ys)


def D(x, y, W, b, context_size, batch_size, dictionary_size, layers):
    """
    Discriminator.
    Classifies batch of input sequences x as real or fake data with respect to the context vectors y.

    :param tf.Tensor x:
        Batch of input sequences that come either from the generator or the real data.
    :param tf.Tensor y:
        Batch of context vectors describing the content of the input sequences x.
    :param dict W:
        Dictionary containing the weight matrix for the output layer with key "out".
    :param dict b:
        Dictionary containing bias vector for the output layer with key "out".
    :param int context_size:
        Size of the context vector.
    :param int batch_size:
        Number of samples.
    :param int dictionary_size:
        Number of words in the vocabulary. This is needed because the generated outputs will be used as inputs in the
        following iterations.
    :param int layers:
        Number of hidden layers.
    :return:
        Batch of float values between 0 and 1, classifying the input vectors as real (1) or fake (0).
    """
    # split x to list of 54 batches, the first batch contains all the first words, the second batch contains all the
    # second words an so on.
    sequence = tf.unstack(x)

    # multi layered neural network with LSTM cells and dictionary_size neurons per layer
    multi_lstm_cell = rnn.MultiRNNCell([rnn.BasicLSTMCell(dictionary_size + context_size)] * layers)

    # initialize LSTM state
    # it's not necessary to look at the state sizes for all layers, because the state sizes are all the same
    (hidden_state_size, current_state_size) = multi_lstm_cell.state_size[0]
    state = [(tf.zeros([batch_size, hidden_state_size]), tf.zeros([batch_size, current_state_size]))] * layers

    output = None
    # feed sequence to network element-wise
    for input in sequence:
        # concat context vector to current element in sequence
        input = tf.reshape(input, [batch_size, dictionary_size])
        input = tf.concat([input, y], 1)
        # get next output and state
        (output, state) = multi_lstm_cell(input, state)
        # multiply networks output with weights and add bias in the output layer
    output = tf.matmul(output, W['out']) + b['out']
    return output


def data_set(data_set_path, dictionary):
    """
    Loads data set from tsv-file.

    :param str data_set_path:
        Path to tsv file containing the data set. The first column of the data set contains the texts and the remaining
        ones the context information.
    :param dict dictionary:
        Mapping of words (str) to indices (int).
    :return:
        Numpy array containing tuples where the first entry is a list of integers representing the text and the second
        entry is a tuple of 16 integer values representing the corresponding context.
    """
    with open(data_set_path) as f:
        # read data from file
        reader = csv.reader(f, delimiter='\t')
        data_set = list(reader)
        # map words and punctuation characters to their corresponding indices
        texts = [list(map(lambda t: dictionary[t], tokens(entry[0]))) for entry in data_set]
        # parse context vectors from file
        contexts = [tuple(map(int, entry[1:])) for entry in data_set]
        data_set_tuples = list(zip(texts, contexts))
        f.close()

    return np.array(data_set_tuples)


def event_type_vectors(indices):
    '''
    Creates two-hot-vectors for each tuple of event types.

    :param np.array indices:
        Array of tuples representing the event types.
    :return:
        Batch of two-hot-vectors representing the given event type tuples.
    '''
    # total number of possible assignments for the event types
    vector_size = 17
    # number of vectors
    n = len(indices)
    vector = np.zeros((n, vector_size))
    (event_type_indices, event_type2_indices) = unzip(indices)
    vector[np.arange(n), event_type_indices] = 1
    vector[np.arange(n), event_type2_indices] = 1
    return vector


def G(z, y, W, b, context_size, batch_size, dictionary_size, layers):
    """
    Generator.
    Generates batch of sequences of word vectors.

    :param tf.Tensor z:
        Batch of noise vectors that are used as inputs in the first step.
    :param tf.Tensor y:
        Batch of context vectors containing information about the generated sequences.
    :param dict W:
        Dictionary containing the weight matrix for the output layer with key "out".
    :param dict b:
        Dictionary containing bias vector for the output layer with key "out".
    :param int context_size:
        Size of the context vector.
    :param int batch_size:
        Number of samples.
    :param int dictionary_size:
        Number of words in the vocabulary. This is needed because the generated outputs will be used as inputs in the
        following iterations.
    :param int layers:
        Number of hidden layers.
    :return:
        Batch of generated sequences as a list of tensors.
    """
    # concat noise vector and context vector
    zy = tf.concat([z, y], 1)

    # Multi layered neural network with LSTM cells and dictionary_size neurons per layer
    multi_lstm_cell = rnn.MultiRNNCell([rnn.BasicLSTMCell(dictionary_size + context_size)] * layers)

    # initialize LSTM state
    # it's not necessary to look at the state sizes for all layers, because the state sizes are all the same
    (hidden_state_size, current_state_size) = multi_lstm_cell.state_size[0]
    state = [(tf.zeros([batch_size, hidden_state_size]), tf.zeros([batch_size, current_state_size]))] * layers

    # calculate first output with noise vector as input
    (output, state) = multi_lstm_cell(zy, state)
    output = tf.sigmoid(tf.matmul(output, W['out']) + b['out'])
    outputs = [output]

    # calculate the next remaining outputs
    for _ in range(53):
        input = tf.concat([output, y], 1)
        (output, state) = multi_lstm_cell(input, state)
        output = tf.nn.sigmoid(tf.matmul(output, W['out']) + b['out'])
        outputs.append(output)

    return tf.stack(outputs)


def load_tensor(path):
    '''
    Loads Numpy array from npy file and returns a Variable that is initialized with the values from the Numpy array.

    :param str path:
        Path to npy file.
    :return:
        Variable that is initialized with the values from the Numpy array.
    '''
    
    W_array = np.load(path, None, False, False)
    W = tf.Variable(W_array)
    return W


def noise(n, noise_size):
    """
    Generates n uniformly distributed random noise samples of shape noise_size in the interval [-1, 1].

    :param int n:
        Number of generated noise samples.
    :param noise_size:
        Size of the noise samples.
    :return:
        Array containing the n generated noise vectors.
    """
    # uniform means all the values got the same probability to be drawn
    return np.random.uniform(0, 1, (n, noise_size))


def one_hot_vectors(indices, shape, size):
    '''
    Creates one-hot-vectors for each index in the indices array.

    :param np.array indices:
        Array of indices that will be represented by the vectors.
    :param shape:
        Output vector's shape. This is used to obtain a batch of one-hot-vectors.
    :param size:
        The length of a single one-hot-vector.
    :return:
        Batch of one-hot-vectors representing the given indices.
    '''
    n = reduce(mul, shape)
    vector = np.zeros((n, size))
    vector[np.arange(n), indices] = 1
    vector = vector.reshape(shape + (size,))
    return vector


def random_body_part(event_type):
    """
    Generates a ID for the body part random variable subject to the previously drawn event type.
    There are three body parts in total ([1, 3]) and an additional value if the information is not available (-1).

    ==  =============
    ID  body part
    ==  =============
    -1  not available
    0   right foot
    1   left foot
    2   head
    ==  =============

    :param int event_type:
        ID of the first event.
    :return:
        Random ID representing a body part.
    """
    # event 1 (attempt) does not exist without a body part
    if event_type == 1:
        return random.randint(1, 3)
    else:
        return random.choice([-1] + list(range(1, 4)))


def random_context(n, event_types_only, data=None):
    """
    Generates *n* random context vectors if data is None. Otherwise draws *n* random context vectors from the given
    data set. If event_types_only is true the context vector will only contain the event type variables.

    Each context vector contains the following 16 random variables:

    +-------------+----------------------------------------------------------------------+
    |variable     |description                                                           |
    +=============+======================================================================+
    |event_type   | first part of the event description                                  |
    +-------------+----------------------------------------------------------------------+
    |event_type2  | second part of the event description                                 |
    +-------------+----------------------------------------------------------------------+
    |event_team   | first team participating in the event                                |
    +-------------+----------------------------------------------------------------------+
    |opponent     | second team participating in the event                               |
    +-------------+----------------------------------------------------------------------+
    |player       | first player participating in the event                              |
    +-------------+----------------------------------------------------------------------+
    |player2      | second player participating in the event                             |
    +-------------+----------------------------------------------------------------------+
    |player_in    | player who enters the field (substitution)                           |
    +-------------+----------------------------------------------------------------------+
    |player_out   | player who leaves the field (substitution)                           |
    +-------------+----------------------------------------------------------------------+
    |shot_place   | direction of the shot                                                |
    +-------------+----------------------------------------------------------------------+
    |shot_outcome | shot outcome                                                         |
    +-------------+----------------------------------------------------------------------+
    |is_goal      | shot goal?                                                           |
    +-------------+----------------------------------------------------------------------+
    |location     | position on the field                                                |
    +-------------+----------------------------------------------------------------------+
    |body_part    | used body part                                                       |
    +-------------+----------------------------------------------------------------------+
    |assist_method| describes way of assistance for the first player by another player.  |
    |             | (0 - None, 1 - Pass, 2 - Cross, 3 - Headed pass, 4 - Through ball)   |
    +-------------+----------------------------------------------------------------------+
    |situation    | describes special situations like free kicks                         |
    +-------------+----------------------------------------------------------------------+
    |fast_break   | fast break after situation?                                          |
    +-------------+----------------------------------------------------------------------+

    :param int n:
        Number of context vectors.
    :param bool event_types_only:
        If this is true the context vectors will be represented as two-hot-vectors.
        The context vector must not consist of other variables than the event types.
    :param np.array data:
        Data set from where to draw the samples.
    :return:
        List of *n* random contexts. Each context is a integer tuple of length 16.
    """
    if data is None:
        contexts = list()
        event_tuples = random_events(n)
        for (event_type, event_type2) in event_tuples:
            teams = random_teams()
            players = random_players(event_type, event_type2)
            shot_place = random_shot_place(event_type2)
            shot_outcome = random_shot_outcome(event_type2)
            is_goal = random_goal(event_type, event_type2)
            location = random_location(event_type)
            body_part = random_body_part(event_type)
            assist_method = random.randint(0, 4)
            situation = random_situation(event_type)
            fast_break = random_fast_break(event_type2)

            context = np.array([event_type, event_type2]
                               + list(teams)
                               + list(players)
                               + [shot_place, shot_outcome, is_goal, location, body_part, assist_method,
                                  situation, fast_break])
            contexts.append(context)
    else:
        # randomize the data order and then draw the first batch_size elements
        np.random.shuffle(data)
        batch = data[:n]
        # divide data in two arrays: (texts, contexts)
        (_, contexts) = unzip(batch)
        # represent event types as two-hot-vectors
        if event_types_only:
            contexts = event_type_vectors(contexts)

    return contexts


def random_events(n):
    """
    Generates *n* tuples (event_type, event_type2).

    ==  ===================
    ID  event_type
    ==  ===================
    0   Announcement
    1   Attempt
    2   Corner
    3   Foul
    4   Yellow card
    5   Second yellow card
    6   Red Card
    7   Substitution
    8   Free kick won
    9   Offside
    10  Hand ball
    11  Penalty
    ==  ===================

    ==  ====================
    ID  event_type2
    ==  ====================
    12  Key Pass
    13  Failed through ball
    14  Sending off
    15  Own goal
    -1  Not available
    ==  ====================

    event_type2 is drawn subject to the first event type, because not all combinations of event_types make sense.

    :param int n:
        Number of drawn event tuples.
    :return:
        List of integer tuples (event_type, event_type2).
    """
    event_tuples = list()
    for _ in range(n):
        event_type = random.randint(-1, 11)
        event_type2 = -1

        if event_type == 1:
            event_type2 = random.choice([-1, 12, 15])
        elif event_type == 4:
            event_type2 = random.choice([-1, 14])
        elif event_type in [5, 6]:
            event_type2 = 14
        elif event_type == 9:
            event_type2 = random.choice([-1, 13])

        event_tuples.append((event_type, event_type2))

    return event_tuples


def random_fast_break(event_type2):
    """
    Generates random fast break value. The value is either 0 or 1 and depends on the second event type.
    An own goal (15) is never followed by a fast break. In this case the result is always 0.

    ==  ==========
    ID  fast_break
    ==  ==========
    0   no
    1   yes
    ==  ==========

    :param int event_type2:
        ID for second event.
    :return:
        0 if the second event type is an own goal (15). Otherwise it's either 0 or 1.
    """
    fast_break = 0
    if event_type2 != 15:
        fast_break = random.randint(0, 1)
    return fast_break


def random_goal(event_type, event_type2):
    """
    Generates either 0 or 1 randomly regarding the both event types.

    ==  =================================================
    ID  is_goal
    ==  =================================================
    -1  no information available (in case of an own goal)
    0   no
    1   yes
    ==  =================================================

    :param int event_type:
        ID for first event.
    :param int event_type2:
        ID for second event.
    :return:
        If the first event type is either attempt (1) or foul (3) the result is 0 or 1.
        If the second event type is own goal (15) the result is not available (-1).
        For all other event types the result is 0.
    """
    is_goal = 0
    # an own goal is the only possibility were the information is not available (-1)
    if event_type2 == 15:
        is_goal = -1
    elif event_type in [1, 3]:
        is_goal = random.randint(0, 1)
    return is_goal


def random_location(event_type):
    """
    Generates random location information regarding the first event type.


    ==  =======================================
    ID  location
    ==  =======================================
    -1  Not available
    1   Attacking half
    2   Defensive half
    3   Centre of the box
    4   Left wing
    5   Right wing
    6   Difficult angle and long range
    7   Difficult angle on the left
    8   Difficult angle on the right
    9   Left side of the box
    10  Left side of the six yard box
    11  Right side of the box
    12  Right side of the six yard box
    13  Very close range
    14  Penalty spot
    15  Outside the box
    16  Long range
    17  More than 35 yards
    18  More than 40 yards
    19  Not recorded
    ==  =======================================

    :param int event_type:
        ID for the first event.
    :return:
        Random integer describing the location.
        For events attempt (1) and free kick won (8) the location must be available.
        For all other events the location can be not available (-1).
    """
    if event_type in [1, 8]:
        return random.randint(1, 19)
    else:
        return random.choice([-1] + list(range(1, 20)))


def random_players(event_type, event_type2):
    """
    Generates random quadruple (player, player2, player_in, player_out) where each random variable is drawn from the
    interval [0, 5855 (total number of players in data set)] or is not available (-1).

    :param int event_type:
        Integer describing the first event type.
    :param int event_type2:
        Integer describing the second event type.
    :return:
        Integer quadruple (player, player2, player_in, player_out).

        In case of a substitution (7) player and player2 are not available (7), instead player_in and player_out are
        drawn randomly.

        For a key pass and an own goal, a second player is always needed. player_in and player_out are not available.
        For all other events the first player is necessary whereas the second player can be not available.
    """
    # draw substitution variables if event_type is substitution (7)
    if event_type == 7:
        player_in = random.randint(0, 5855)
        # player can't be substituted by himself
        player_out_list = list(range(0, 5856))
        player_out_list.remove(player_in)
        player_out = random.choice(player_out_list)
        return (-1, -1, player_in, player_out)
    elif event_type2 in [12, 15]:
        player = random.randint(0, 5855)
        # key pass (12) and own goal (15) require a second player
        player2_list = list(range(0, 5856))
        player2_list.remove(player)
        player2 = random.choice(player2_list)
        return (player, player2, -1, -1)
    else:
        player = random.randint(0, 5855)
        # for all other events the second player can be NA (-1)
        player2_list = list(range(-1, 5856))
        player2_list.remove(player)
        player2 = random.choice(player2_list)
        return (player, player2, -1, -1)


def random_shot_outcome(event_type2):
    """
    Generates a random value for the shot outcome depending on the second event type.

    ==  ================
    ID  shot_outcome
    ==  ================
    -1  Not available
    1   On target
    2   Off target
    3   Blocked
    4   Hit the bar
    ==  ================

    :param int event_type2:
        Integer describing the second event type.
    :return:
        In case of the second event type being a key pass (12) the shot outcome must be available.

        For all other events the shot outcome can also be not available (-1).
    """
    # key pass (12) requires a shot outcome
    if event_type2 == 12:
        return random.randint(1, 4)
    else:
        return random.choice([-1] + list(range(1, 5)))


def random_shot_place(event_type2):
    """
    Generates a random value for the shot place depending on the second event type.

    ==  =======================
    ID  shot_place
    ==  =======================
    -1  Not available
    1   Bit too high
    2   Blocked
    3   Bottom left corner
    4   Bottom right corner
    5   Centre of the goal
    6   High and wide
    7   Hits the bar
    8   Misses to the left
    9   Misses to the right
    10  Too high
    11  Top centre of the goal
    12  Top left corner
    13  Top right corner
    ==  =======================

    :param int event_type2:
        Integer describing the second event type.
    :return:
        Returns a random integer describing the shot place.

        In case of the second event type being a key pass (12) or an own goal (15) the shot place must be available.

        For all other events the shot plave can be unavailable (-1).
    """
    # key pass (12) and own goal require a shot place
    if event_type2 in [12, 15]:
        return random.randint(1, 13)
    else:
        return random.choice([-1] + list(range(1, 14)))


def random_situation(event_type):
    """
    Generates a random situation depending on the first event type.

    ==  =============
    ID  situation
    ==  =============
    -1  Not available
    1   Open play
    2   Set piece
    3   Corner
    4   Free kick
    ==  =============

    :param int event_type:
        Integer describing the first event type.
    :return:
        Integer describing a situation.

        In case of the first event being an attempt (1) the situation must be available.

        For all other events the situation can be unavailable.
    """
    if event_type == 1:
        return random.randint(1, 4)
    else:
        return random.choice([-1] + list(range(1, 5)))


def random_teams():
    """
    Generates a random integer tuple (event_team, opponent) where event_team and opponent can't be the same.

    event_team and opponent are drawn from the interval [0, 144 (total number of clubs in the data set)].

    :return:
        Integer tuple describing the event team and it's opponent.
    """
    event_team = random.randint(0, 144)
    # remove event team form opponent list because a club can't be it's own opponent
    opponent_list = list(range(0, 145))
    opponent_list.remove(event_team)
    opponent = random.choice(opponent_list)
    return (event_team, opponent)


def seq2vec(sequence, dictionary):
    """
    Takes a String of elements of the dictionary and maps it to the corresponding indices.

    :param str sequence:
        String containing elements of the dictionary, separated by spaces.
    :param dict dictionary:
        Dictionary that maps elements of the alphabet (str) to their corresponding index (int).
    :return:
        Array of corresponding indices.
    """
    # map all elements of the sequence to their corresponding indices
    sequence_indices = list(map(lambda x: dictionary[x], tokens(sequence)))
    return np.array(sequence_indices)


def tokens(text):
    '''
    Read all words and punctuation characters from string.

    :param str text:
        Text.
    :return:
        List of all words and punctuation characters that occur in the text.
    '''
    tokens = re.findall(r"[\w']+|[$.,!?;()]", text)
    return tokens


def unzip(tuples):
    '''
    Takes an array of tuples and returns two arrays, one containing all the first elements and another containing all
    the second elements of the tuples.

    :param np.array tuples:
        Array of tuples.
    :return:
        Two arrays, one containing all the first elements and another containing all the second elements of the tuples.
    '''
    tuples = list(zip(*tuples))
    return (np.array(tuples[0]), np.array(tuples[1]))


def vec2seq(indices, reverse_dictionary):
    """
    Maps an array of indices to their corresponding words.

    :param np.array vectors:
        Array containing indices representing sequence elements. The array represents batch_size sentences.
    :param dict reverse_dictionary:
        Dictionary mapping indices to their corresponding words.
    :return:
        List of strings that are represented by the input vectors
    """
    (sequence_length, batch_size, _) = np.shape(indices)
    # initialize batch_size strings
    sequences = [""] * batch_size
    # extract all words for each batch and build sentences
    for i in range(sequence_length):
        for j in range(batch_size):
            index = np.argmax(indices[i][j])
            word = reverse_dictionary[index]
            sequences[j] = sequences[j] + " " + word

    return sequences


def vocabulary(vocabulary_path):
    '''
    Read vocabulary containing words and punctuation characters from file.

    :param str vocabulary_path:
        Path to file.
        Expects a tsv file where the first column contains all the tokens and the second column contains
        the frequency, even if the frequency is not needed.
    :return:
        List of all words in vocabulary.
    '''
    with open(vocabulary_path) as f:
        reader = csv.reader(f, delimiter='\t')
        (tokens, _) = unzip(map(tuple, reader))
        f.close()
    return tokens


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Run the CGARNN model on the football events data with indices representing the sequence elements.")
    parser.add_argument("vocabulary_path", metavar="FREQUENCY_PATH", type=str,
                        help="path to tsv file containing words and their frequencies")
    parser.add_argument("data_set_path", metavar="DATA_SET_PATH", type=str,
                        help="path to tsv file containing texts and their corresponding labels."
                             "NOTE: if the data set does not contain all 16 context variables, the random context"
                             "vectors are drawn from the data set automatically")
    parser.add_argument("--gweights", metavar="PATH", type=str, default=None,
                        help="path to npy file containing the generator's weights")
    parser.add_argument("--gbias", metavar="PATH", type=str, default=None,
                        help="path to npy file containing the generator's bias")
    parser.add_argument("--dweights", metavar="PATH", type=str, default=None,
                        help="path to npy file containing the discriminator's weights")
    parser.add_argument("--dbias", metavar="PATH", type=str, default=None,
                        help="path to npy file containing the discriminator's bias")
    parser.add_argument("--snapshot", action="store_true", help="save weights and biases after training")
    parser.add_argument("--fromdata", action="store_true",
                        help="if True, the random context will be drawn form the data set rather then generating it (default: False)")
    parser.add_argument("--etonly", action="store_true",
                        help="only use this flag when the given data set contains nothing but event_type and "
                             "event_type2 as context information. the context will then be represented as a two-hot-vector")
    parser.add_argument("--output", metavar="PATH", default="output.txt", type=str,
                        help="path to output file for generated sequences (default: output.txt)")
    parser.add_argument("--loss", metavar="PATH", default="loss.csv", type=str,
                        help="path to csv file for logging loss values (default: loss.csv)")
    parser.add_argument("--log", metavar="PATH", default="log.txt", type=str,
                        help="path to file for logging time measurement (default: log.txt)")
    parser.add_argument("--epochs", metavar="INTEGER", default=50000, type=int,
                        help="number of training iterations (default: 50000)")
    parser.add_argument("-k", metavar="INTEGER", default=1, type=int,
                        help="number of discriminator's training steps per training step of the generator (default: 1)")
    parser.add_argument("--batchsize", metavar="INTEGER", default=64, type=int,
                        help="size of mini-batch (default: 64)")
    parser.add_argument("--glayers", metavar="INTEGER", default=5, type=int,
                        help="number of generator's LSTM layers (default: 5)")
    parser.add_argument("--dlayers", metavar="INTEGER", default=3, type=int,
                        help="number of discriminator's LSTM layers (default: 3)")
    parser.add_argument("--galpha", metavar="FLOAT", default=0.001, type=float,
                        help="generator's learning rate (default: 0.001)")
    parser.add_argument("--dalpha", metavar="FLOAT", default=0.001, type=float,
                        help="discriminator's learning rate (default: 0.001)")

    args = parser.parse_args()

    # path to file containing all words of the data set and their frequencies
    vocabulary_path = args.vocabulary_path
    # path to tsv file containing the football event texts and their labels
    data_set_path = args.data_set_path
    # path to text file for generated sequences
    output_path = args.output
    # path to csv file for logging loss values
    loss_path = args.loss
    # path to text file for logging time measurement
    log_path = args.log
    # either generate real random contexts or draw a random context from the data set
    from_data = args.fromdata
    # the context will be represented as a two-hot-vector. event types only!
    event_types_only = args.etonly
    # number of training iterations
    # ca 45 minutes per 1000 epochs
    epochs = args.epochs
    # discriminator's number of training steps per generator's training iteration
    k = args.k
    # size of mini-batch
    batch_size = args.batchsize
    # generator's learning rate
    G_alpha = args.galpha
    # number of generator's LSTM layers
    G_layers = args.glayers
    # discriminator's learning rate
    D_alpha = args.dalpha
    # number of discriminator's LSTM layers
    D_layers = args.dlayers
    # paths to generator's and discriminator's weights and biases
    G_weights_path = args.gweights
    G_bias_path = args.gbias
    D_weights_path = args.dweights
    D_bias_path = args.dbias
    # save weights and biases after training if snapshot is True
    snapshot = args.snapshot

    print("Loading vocabulary... ", end='')
    # list of all words in file
    vocab = vocabulary(vocabulary_path)
    print("Done!")

    print("Building vocabulary dictionaries... ", end='')
    # maps words to indices
    dictionary = dict()
    for word in vocab:
        dictionary[word] = len(dictionary)
    # maps indices to words
    reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
    dictionary_size = len(dictionary)
    print("Done!")

    print("Deleting vocabulary... ", end='')
    del vocab
    print("Done!")

    print("Loading data set... ", end='')
    data = data_set(data_set_path, dictionary)
    print("Done!")

    # get context size from data set
    context_size = len(data[0][1])

    # if the context size is not 16, the random context vector cannot be generated and must be drawn from the data set
    if context_size != 16:
        from_data = True
        print("Context size %d differs from standard size 16: Random context vectors will be drawn from the data set."
              % context_size)

    # check if context only contains two variables
    if event_types_only and context_size != 2:
        print("Context size %d is not 2! Use a data set that contains only event_type and event_type2 "
              "as context information when using --etonly." % context_size)
        exit(1)
    elif event_types_only:
        # size for two-hot-vector
        context_size = 17

    print("Building network graphs... ", end='')
    seed = 611357224
    xavier = tf.glorot_normal_initializer(seed=seed)

    # Discriminator's input. None means that there can be an arbitrary number of input vectors. Each input vector must be of
    # size dictionary_size, because the words are represented by bag-of-wordss and their size is determined by the
    # dictionary size
    x = tf.placeholder(tf.float32, [54, batch_size, dictionary_size], "x")

    # Context input for both generator and discriminator. None means that there can be an arbitrary number of context
    # vectors. Each context vector must be of size 16 because there are 16 entries each represented by a numerical value
    # in the data set that form the context.
    y = tf.placeholder(tf.float32, [batch_size, 16], "y")

    # Generator's input. None means that there can be an arbitrary number of input vectors. Each input vector must be of
    # size dictionary_size, because the network's outputs will be used as the input in the next iteration.
    z = tf.placeholder(tf.float32, [batch_size, dictionary_size], "z")

    if G_weights_path is not None:
        G_W_out = load_tensor(G_weights_path)
    else:
        G_W_out = tf.get_variable("G_W", [dictionary_size + 16, dictionary_size], initializer=xavier)
    # Generator's output layer weights
    G_W = {'out': G_W_out}

    if G_bias_path is not None:
        G_b_out = load_tensor(G_bias_path)
    else:
        G_b_out = tf.get_variable("G_b", [dictionary_size], initializer=xavier)
    # Generator's output layer bias
    G_b = {'out': G_b_out}

    if D_weights_path is not None:
        D_W_out = load_tensor(D_weights_path)
    else:
        D_W_out = tf.get_variable("D_W", [dictionary_size + 16, 1], initializer=xavier)
    # Discriminator's output layer weights
    D_W = {'out': D_W_out}

    if D_bias_path is not None:
        D_b_out = load_tensor(D_bias_path)
    else:
        D_b_out = tf.get_variable("D_b", [1], initializer=xavier)
    # Discriminator's output layer bias
    D_b = {'out': D_b_out}

    # Generator's output sequence
    G_seq = G(z, y, G_W, G_b, context_size, batch_size, dictionary_size, G_layers)

    # Discriminator's output for fake and real inputs
    # D_real and D_fake must share its RNN variables because they represent the same network with different inputs, this is
    # why we use a variable scope D here
    with tf.variable_scope("D") as scope:
        D_real = D(x, y, D_W, D_b, context_size, batch_size, dictionary_size, D_layers)
        scope.reuse_variables()
        D_fake = D(G_seq, y, D_W, D_b, context_size, batch_size, dictionary_size, D_layers)

    # Generator's loss and optimizer function
    G_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=D_fake, labels=tf.ones_like(D_fake)))
    G_optimizer = tf.train.AdamOptimizer(learning_rate=G_alpha).minimize(G_loss, var_list=list(G_W.values()) + list(
        G_b.values()))

    # Discriminator's loss and optimizer function
    D_loss_real = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=D_real, labels=tf.ones_like(D_real)))
    D_loss_fake = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(logits=D_fake, labels=tf.zeros_like(D_fake)))
    D_loss = D_loss_fake + D_loss_fake
    D_optimizer = tf.train.AdamOptimizer(learning_rate=D_alpha).minimize(D_loss, var_list=list(D_W.values()) + list(
        D_b.values()))
    print("Done!")

    print("Initializing variables... ", end='')
    sess = tf.InteractiveSession()
    tf.global_variables_initializer().run()
    print("Done!")

    print("Training network...\n")
    loss_file = open(loss_path, 'w+')
    output_file = open(output_path, 'w+')

    # start time measurement
    t_start = time.time()

    for e in range(epochs):
        # train discriminator for k iterations
        (xs, ys) = (None, None)
        for _ in range(k):
            # generate batch
            (xs, ys) = batch(batch_size, data, event_types_only)
            sess.run(D_optimizer, {x: xs, y: ys, z: noise(batch_size, dictionary_size)})

        # generate random context vectors
        if from_data:
            ys = random_context(batch_size, event_types_only, data)
        else:
            ys = random_context(batch_size, event_types_only)
        # train generator for one iteration
        sess.run(G_optimizer, {y: ys, z: noise(batch_size, dictionary_size)})

        # log loss and generated sequences every 1000th iteration
        if e % 1000 == 0:
            seq = sess.run(G_seq, {y: ys, z: noise(batch_size, dictionary_size)})
            (g_loss, d_loss) = sess.run([G_loss, D_loss], {x: xs, y: ys, z: noise(batch_size, dictionary_size)})
            print("Epoch:\t %d" % e)
            print("Generator's loss:\t %f" % g_loss)
            print("Discriminator's loss:\t %f\n" % d_loss)
            loss_file.write("%d, %f, %f\n" % (e, g_loss, d_loss))

            # map vectors to sequences
            sequences = vec2seq(seq, reverse_dictionary)
            for sequence in sequences:
                output_file.write(sequence + "\n")

    # stop time measurement
    t_end = time.time()
    t_duration = t_end - t_start
    print("Done!")

    with open(log_path, 'w+') as log_file:
        t_duration_secs = t_duration
        log_file.write("training duration: %f seconds" % t_duration_secs)
        log_file.close()

    loss_file.close()
    output_file.close()

    # save weights and biases for further use
    if snapshot:
        np.save("gweights.npy", G_W['out'].eval(), False, False)
        np.save("gbias.npy", G_b['out'].eval(), False, False)
        np.save("dweights.npy", D_W['out'].eval(), False, False)
        np.save("dbias.npy", D_b['out'].eval(), False, False)

    exit(0)
