import argparse
import csv
import matplotlib.pyplot as plt
import numpy as np
from sklearn.manifold import TSNE
import tensorflow as tf


def embeddings(embeddings_path):
    """
    Loads embedding matrix from file.

    :param str embeddings_path:
        Path to npy file containing the embedding matrix.
    :return:
        Quadruple containing the following elements:

        - **embed**
            Tensor representing the loaded embeddings matrix.

        - **reverse_embed**
            Dictionary mapping word embeddings (as tuples) back to their corresponding index.

        - **embed_array**
            Array representing the loaded embeddings matrix.

        - **embeddings_size**
            Size of word embeddings.
    """
    # load embeddings matrix from file and convert it to a tensor
    embed_array = np.load(embeddings_path, None, False, False)
    embed = tf.constant(embed_array)

    # build dictionary for reverse embeddings
    reverse_embed = dict()
    (vocabulary_size, embeddings_size) = np.shape(embed_array)
    for i in range(vocabulary_size):
        # convert numpy array to tuple and use it as a dictionary key to map vectors to indices
        reverse_embed[tuple(embed_array[i])] = i

    return (embed, reverse_embed, embed_array, embeddings_size)


def plot_embeddings(embeddings, words, reverse_dictionary, plot_path):
    '''
    Plots 2-dimensional representation of learned word embeddings and writes the plot to the specified file.

    :param np.array embeddings:
        2-dimensional representation of embeddings.
        Embeddings dimension must be changed in order to make plotting possible.
    :param [int] words:
        List of indices representing words to be included in the plot. These words are used as labels for the
        plotted points.
    :param dict reverse_dictionary:
        Dictionary that maps indices to there corresponding word.
    :param str plot_path:
        Path to file in which the plot will be saved.
    :return:
        *None*
    '''

    # prepare plot of size 32 x 32
    plt.figure(figsize=(32, 32))
    for word in words:
        # get 2-dimensional vector representation of the current word
        (x, y) = embeddings[word]
        # add (x, y) to plot
        plt.scatter(x, y)
        # add word as label to point (x, y)
        plt.annotate(reverse_dictionary[word], xy=(x, y), xytext=(5, 2), textcoords='offset points', ha='right', va='bottom')

    plt.savefig(plot_path)


def unzip(tuples):
    '''
    Takes an array of tuples and returns two arrays, one containing all the first elements and another containing all
    the second elements of the tuples.

    :param np.array tuples:
        Array of tuples.
    :return:
        Two arrays, one containing all the first elements and another containing all the second elements of the tuples.
    '''
    tuples = list(zip(*tuples))
    return (np.array(tuples[0]), np.array(tuples[1]))


def vocabulary(vocabulary_path):
    '''
    Read vocabulary containing words and punctuation characters from file.

    :param str vocabulary_path:
        Path to file.
        Expects a tsv file where the first column contains all the tokens and the second column contains
        the frequency.
    :return:
        List of all words in vocabulary.
    '''
    with open(vocabulary_path) as f:
        reader = csv.reader(f, delimiter='\t')
        (tokens, _) = unzip(map(tuple, reader))
        f.close()
    return tokens


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Plot word embeddings from npy file.")
    parser.add_argument("embeddings_path", metavar="EMBEDDINGS_PATH", type=str,
                        help="path to npy file containing the embedding matrix")
    parser.add_argument("vocabulary_path", metavar="FREQUENCY_PATH", type=str,
                        help="path to tsv file containing words and their frequencies")
    parser.add_argument("-o", metavar="PATH", default="plot.png", type=str,
                        help="write plot to the specified file (default: plot.png)")
    parser.add_argument("-n", metavar="INTEGER", default=None, type=int,
                        help="number of words the plot will contain (default: all)")
    args = parser.parse_args()

    embeddings_path = args.embeddings_path
    vocabulary_path = args.vocabulary_path
    plot_path = args.o
    num_plot = args.n

    print("Loading embeddings... ", end='')
    # load embeddings
    (embed, reverse_embed, embed_array, embeddings_size) = embeddings(embeddings_path)
    print("Done!")

    print("Building dictionaries... ", end='')
    # list of all words in file
    vocab = vocabulary(vocabulary_path)
    # maps words to indices
    dictionary = dict()
    for word in vocab:
        dictionary[word] = len(dictionary)
    # maps indices to words
    reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
    print("Done!")

    print("Downscaling embeddings... ", end='')
    tsne = TSNE(perplexity=30, n_components=2, init='pca', n_iter=5000, method='exact')
    two_d_embeddings = tsne.fit_transform(embed_array)
    print("Done!")

    print("Plotting embeddings... ", end='')
    n = len(dictionary)
    if num_plot is None:
        num_plot = n
    elif num_plot < 0:
        print("\n Negative number of words! Using default.")
        num_plot = n
    elif num_plot > n:
        print("\n %d exceeds the total number of words! Using default.")

    plot_embeddings(two_d_embeddings, list(range(num_plot)), reverse_dictionary, plot_path)
    print("Done!")
    exit(0)